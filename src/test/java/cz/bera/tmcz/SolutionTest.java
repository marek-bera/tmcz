package cz.bera.tmcz;

import cz.bera.tmcz.service.Solution;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SolutionTest {

  private static final String path = "./src/test/resources";
  private static final String filename = "/input.txt";

  @Autowired
  private Solution solution;

  @DisplayName("Test file exists")
  @Test
  @Order(1)
  public void fileExists_test() {
    File file = new File(path, filename);

    assertTrue(file.exists());
  }

  @DisplayName("Test solution")
  @Test
  @Order(2)
  public void solution_test() {
    String result = this.solution.solution(path + filename);

    String expected = "Netflix|02|payment weekly\n" +
        "Apple|1|game Of Thrones\n" +
        "Netflix|01|payment yearly\n" +
        "Microsoft|2|Office 365\n" +
        "Microsoft|1|Office 365\n" +
        "Apple|2|payment weekly\n" +
        "Microsoft|3|application Any.DO\n" +
        "Netflix|03|new subscription\n" +
        "Netflix|09|installation of Modem\n" +
        "Netflix|07|Sport 02\n" +
        "Netflix|06|monthly subscription\n" +
        "Netflix|08|O2TV, SportTV\n" +
        "Netflix|04|game Of thrones\n" +
        "Netflix|05|yearly subscription\n" +
        "Netflix|10|recharging of 987654321";

    assertEquals(expected, result);
  }
}
