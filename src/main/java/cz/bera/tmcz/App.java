package cz.bera.tmcz;

import cz.bera.tmcz.service.Solution;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class App implements CommandLineRunner {

  @Autowired
  private Solution solution;

  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }

  @Override
  public void run(String... args) {
    log.info("Executing command line runner with {} arguments", args.length);
    if (args.length > 0) {
      String solution = this.solution.solution(args[0]);
      log.info("\n{}", solution);
    }
  }
}
