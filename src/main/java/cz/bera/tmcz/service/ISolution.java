package cz.bera.tmcz.service;

public interface ISolution {

  String solution(String location);
}
