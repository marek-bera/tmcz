package cz.bera.tmcz.service;

import cz.bera.tmcz.pojo.MyException;
import cz.bera.tmcz.pojo.ResultDO;
import cz.bera.tmcz.pojo.SourceDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
public class Solution implements ISolution {

  private static final int INPUT_LINES_THRESHOLD = 100;
  private static final LocalDateTime DATE_MIN = LocalDateTime.of(2000, 1, 1, 0, 0);
  private static final LocalDateTime DATE_MAX = LocalDate.of(2020, 12, 31).atTime(LocalTime.MAX);

  @Override
  public String solution(String location) {
    List<SourceDO> sourceDOs = new ArrayList<>();
    try {
      List<String> originalStrings = readFile(location);

      IntStream
          .range(0, originalStrings.size())
          .mapToObj(i -> parseLine(originalStrings.get(i), i))
          .collect(Collectors.toCollection(() -> sourceDOs));
      validateInput(sourceDOs);

    } catch (IOException ex) {
      log.error("Unable to read file.\n{}", ex.getMessage());
      throw new MyException(ex.getMessage());
    } catch (NumberFormatException | DateTimeParseException ex) {
      log.error("Unable to parse file.\n{}", ex.getMessage());
      throw new MyException(ex.getMessage());
    } catch (IllegalArgumentException ex) {
      log.error(ex.getMessage());
      throw ex;
    }

    Map<String, List<SourceDO>> groupedByPartner = sourceDOs
        .stream()
        .sorted(Comparator.comparing(SourceDO::getTimeOfTransaction))
        .collect(Collectors.groupingBy(SourceDO::getPartner));

    List<ResultDO> solutionResultDOs = groupedByPartner
        .values()
        .stream()
        .map(sourceDOS -> {
          List<ResultDO> resultDOs = new ArrayList<>();
          for (int i = 0; i < sourceDOS.size(); i++) {
            resultDOs.add(map(sourceDOS.get(i), i + 1, sourceDOS.size()));
          }
          return resultDOs;
        })
        .flatMap(List::stream)
        .sorted(Comparator.comparingInt(ResultDO::getIndex))
        .collect(Collectors.toUnmodifiableList());

    return solutionResultDOs
        .stream()
        .map(ResultDO::toString)
        .collect(Collectors.joining("\n"));

  }

  private ResultDO map(SourceDO sourceDO, int order, int totalCount) {
    return ResultDO
        .builder()
        .index(sourceDO.getIndex())
        .partner(sourceDO.getPartner())
        .transactionOrder(order)
        .nameOfTransaction(sourceDO.getNameOfTransaction())
        .totalCount(totalCount)
        .build();
  }

  private List<String> readFile(String location) throws IOException, MyException {
    Path path = Paths.get(location);

    if (Files.lines(path).count() > INPUT_LINES_THRESHOLD) {
      throw new MyException("Input has more thar " + INPUT_LINES_THRESHOLD + " lines.");
    }
    return Files.readAllLines(path);
  }

  private void validateInput(List<SourceDO> sourceDOs) throws MyException {
    if (sourceDOs
        .stream()
        .anyMatch(sourceDO ->
            sourceDO.getTimeOfTransaction().isBefore(DATE_MIN)
            || sourceDO.getTimeOfTransaction().isAfter(DATE_MAX)
        )
    ) {
      throw new MyException("Invalid transaction date.");
    }

  }

  private SourceDO parseLine(String line, int index) throws NumberFormatException, DateTimeParseException {
    String[] parsed = line.split("[,/](?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    return SourceDO
        .builder()
        .index(index)
        .nameOfTransaction(correctStringValues(parsed[0]))
        .partner(correctStringValues(parsed[1]))
        .clientPhoneNumber(Integer.parseInt(correctStringValues(parsed[2])))
        .timeOfTransaction(LocalDateTime.parse(correctStringValues(parsed[3]), formatter))
        .build();
  }

  private String correctStringValues(String value) {
    return value.replaceAll("\"", "").replaceAll("\\s{2,}", " ").trim();
  }

}
