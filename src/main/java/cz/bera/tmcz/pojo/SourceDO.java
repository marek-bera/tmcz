package cz.bera.tmcz.pojo;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Builder
@Value
public class SourceDO {

  int index;
  String nameOfTransaction;
  String partner;
  long clientPhoneNumber;
  LocalDateTime timeOfTransaction;
}
