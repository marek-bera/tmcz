package cz.bera.tmcz.pojo;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class ResultDO {

  int index;
  String partner;
  int transactionOrder;
  String nameOfTransaction;
  int totalCount;

  @Override
  public String toString() {
    if (totalCount >= 10) {
      return String.format("%s|%02d|%s", partner, transactionOrder, nameOfTransaction);
    }
    return String.format("%s|%d|%s", partner, transactionOrder, nameOfTransaction);
  }
}
